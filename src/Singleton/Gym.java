package Singleton;

public class Gym {
    private static Gym instance = new Gym();

    private Gym(){}

    public static Gym getInstance(){
        return instance;
    }

    public void showGymName(){
        System.out.println("Smartfit");
    }
}
