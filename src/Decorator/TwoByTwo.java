package Decorator;

public class TwoByTwo implements CubeRubik {
    @Override
    public void showDimensions(int number) {
        System.out.println("My dimensions are " + number + " x " + number);
    }

    @Override
    public void quantityPieces(int number) {
        System.out.println("I have " + number * 6 + " pieces");
    }

}
