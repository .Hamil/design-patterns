package Decorator;

public class MultiplyByTwoCubeRubikDecorator extends CubeRubikDecorator {

    public MultiplyByTwoCubeRubikDecorator(CubeRubik cube) {
        super(cube);
    }

    @Override
    public void quantityPieces(int number) {
        super.quantityPieces(number);
    }

    @Override
    public void showDimensions(int number) {
        MultiplyByTwoCubeRubik(number);
    }

    private void MultiplyByTwoCubeRubik(int number){//here we add the new functionality, this what is about the patter design decorator :)
        System.out.println("The Rubik Cube has been multiply by 2, now it's "+ number * 2+" x " + number * 2);
    }
}
