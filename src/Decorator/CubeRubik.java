package Decorator;

public interface CubeRubik {
    void showDimensions(int number);
    void quantityPieces(int number);
}
