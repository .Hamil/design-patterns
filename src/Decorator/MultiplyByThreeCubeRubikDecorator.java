package Decorator;

public class MultiplyByThreeCubeRubikDecorator extends CubeRubikDecorator {

    public MultiplyByThreeCubeRubikDecorator(CubeRubik cube) {
        super(cube);
    }

    @Override
    public void quantityPieces(int number) {
        super.quantityPieces(number);
    }

    @Override
    public void showDimensions(int number) {
        MultiplyByThreeCubeRubik(number);
    }

    private void MultiplyByThreeCubeRubik(int number){//here we add the new functionality, this what is about the patter design decorator :)
        System.out.println("The Rubik Cube has been multiply by 2, now it's "+ number * 3+" x " + number * 3);
    }
}
