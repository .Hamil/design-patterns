package Decorator;

public abstract class CubeRubikDecorator implements CubeRubik {
    protected CubeRubik cube;

    public CubeRubikDecorator(CubeRubik cube){
        this.cube = cube;
    }

    @Override
    public void showDimensions(int number) {
        cube.showDimensions(number);
    }

    @Override
    public void quantityPieces(int number) {
        cube.quantityPieces(number);
    }
}
