package Adapter;

public class LegsMachine implements LegsFrontSideMachine {

    LegsMachineAdapter adapter;
    String sideExercise;

    public LegsMachine(String sideExercise){
        this.sideExercise = sideExercise;
    }

    @Override
    public void showMessage() {
        if(this.sideExercise.equalsIgnoreCase("Front")){
            System.out.println("You're looking for a Quads machine, in this machine you can do Quads");
        }else if(this.sideExercise.equalsIgnoreCase("Back")){
            adapter = new LegsMachineAdapter("Glutes");
            adapter.showMessage();
            adapter = new LegsMachineAdapter("Curl");
            adapter.showMessage();
        }
    }
}
