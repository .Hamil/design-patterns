package Adapter;

public class LegsCurlMachine implements LegsBackSideMachine{
    @Override
    public void showMessage() {
        System.out.println("You're looking for a Curl Legs machine, in this machine you can do Curls");
    }
}
