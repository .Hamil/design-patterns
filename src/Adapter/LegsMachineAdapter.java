package Adapter;

public class LegsMachineAdapter implements LegsFrontSideMachine {

    LegsBackSideMachine backSideMachine;

    public LegsMachineAdapter(String sideExercise){
        if(sideExercise.equalsIgnoreCase("Curl")){
            backSideMachine = new LegsCurlMachine();
        }else if(sideExercise.equalsIgnoreCase("Glutes")){
            backSideMachine = new GlutesMachine();
        }
    }

    @Override
    public void showMessage() {
        backSideMachine.showMessage();
    }
}
