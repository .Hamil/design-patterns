import Decorator.*;
import Adapter.*;
import Singleton.*;
import Factory.*;

public class Main {
    public static void main(String [] args){
        SupplementFactory factory = new SupplementFactory();
        Supplement supplement = factory.getSupplement("Fish Oil");
        supplement.showResearch();
        supplement.showPurpose();
    }
}
