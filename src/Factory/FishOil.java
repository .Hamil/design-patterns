package Factory;

public class FishOil implements Supplement {
    @Override
    public void showResearch() {
        System.out.println("For most other conditions for which omega-3 supplements have been studied, the evidence is inconclusive or doesn’t indicate that omega-3s are beneficial.");
    }

    @Override
    public void showPurpose() {
        System.out.println(" - Research indicates that omega-3 supplements don’t reduce the risk of heart disease. However, people who eat seafood one to four times a week are less likely to die of heart disease.\n" +
                "- High doses of omega-3s can reduce levels of triglycerides.\n" +
                "- Omega-3 supplements may help relieve symptoms of rheumatoid arthritis.\n" +
                "- Omega-3 supplements have not been convincingly shown to slow the progression of the eye disease age-related macular degeneration.\n");
    }
}
