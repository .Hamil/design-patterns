package Factory;

public interface Supplement {
    void showResearch();
    void showPurpose();
}
