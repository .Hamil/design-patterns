package Factory;

public class SupplementFactory {
    public Supplement getSupplement(String supplement){//in this function we put the logic for instanciate the objects
        if(supplement.equalsIgnoreCase("Protein")){
            return new WheyProtein();
        }else if(supplement.equalsIgnoreCase("Creatine")){
            return new Creatine();
        }else if(supplement.equalsIgnoreCase("Fish Oil")){
            return new FishOil();
        }
        return null;
    }
}
