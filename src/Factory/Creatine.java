package Factory;

public class Creatine implements Supplement{
    @Override
    public void showResearch() {
        System.out.println("Creatine is one of the most popular nutritional ergogenic aids for athletes. ");
    }

    @Override
    public void showPurpose() {
        System.out.println("Studies have consistently shown that creatine supplementation increases intramuscular creatine concentrations which may help explain the observed improvements in high intensity exercise performance leading to greater training adaptations. In addition to athletic and exercise improvement, research has shown that creatine supplementation may enhance post-exercise recovery, injury prevention, thermoregulation, rehabilitation, and concussion and/or spinal cord neuroprotection.");
    }
}
