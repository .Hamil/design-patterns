package Factory;

public class WheyProtein implements Supplement{
    @Override
    public void showResearch() {
        System.out.println("Current research supports the consumption of a moderate dose (~20–25 g) of rapidly digested, leucine-rich proteins to optimize muscle protein synthesis [3,4,5]; this ostensibly positions whey protein as a valuable supplemental source for individuals aiming to maximize their recovery from and adaptation to resistance exercise");
    }

    @Override
    public void showPurpose() {
        System.out.println("Consuming a source of protein after resistance exercise is essential to maximize muscle protein synthesis and net protein balance [1,2], both of which are required to support muscle hypertrophy with training.");
    }
}
